#include <genieArduino.h>

#define PIN_LED 8
#define PIN_RELAY 3
#define PIN_SCREEN 4
#define PIN_SWITCH 2

Genie genie;
bool reward_available;
int testN;

void setLED(bool on) {
    digitalWrite(PIN_LED, on ? HIGH : LOW);
}

void setScreen(bool on) {
    genie.WriteContrast(on ? 15 : 0);
}

void dispenseReward() {
    if (!reward_available) {
        digitalWrite(PIN_RELAY, HIGH);
        delay(175);
        digitalWrite(PIN_RELAY, LOW);
    
        reward_available = true;
    }
}

void setup() {
    reward_available = false;
    testN = 1;
  
    Serial.begin(9600);
    genie.Begin(Serial);
 
    pinMode(PIN_LED, OUTPUT);
    pinMode(PIN_RELAY, OUTPUT);
    pinMode(PIN_SCREEN, OUTPUT);

    digitalWrite(PIN_SCREEN, HIGH);
    delay(100);
    digitalWrite(PIN_SCREEN, LOW);
    delay(3000);
}

void loop() {
    unsigned long start = millis();
    int hits = 0;
    
    while (true) {
        int now = millis() - start;
        if (now % 1000 == 0) {
            if (now == 1000) {
                setScreen(true);
            }
            else if (now == 2000) {
                setLED(true);
            }
            else if (now == 3000) {
                dispenseReward();
            }
            else if (now > 10000 && now < 40000) {
                setScreen(false);
                setLED(false);
            }
            else if (now > 40000) {
                ++testN;
                break;
            }
        }

        int hit = analogRead(PIN_SWITCH);
        if (hit == 1023) {
            ++hits;
        }
        else {
            hits = 0;
        }  
        if (hits == 100 && reward_available) {
            reward_available = false;
            Serial.println(testN);
        }
    }
}
