#include <genieArduino.h>

#define PIN_LED 8
#define PIN_RELAY 3
#define PIN_SCREEN 4
#define PIN_SWITCH 2

Genie genie;
bool accept_input;
bool reward_available;
int testN;

void handleButton() {
    genieFrame Event;
    genie.DequeueEvent(&Event);

    if (accept_input && !reward_available) {
        digitalWrite(PIN_RELAY, HIGH);
        delay(150);
        digitalWrite(PIN_RELAY, LOW);

        digitalWrite(PIN_LED, HIGH);
        genie.WriteContrast(0);
        accept_input = false;
        reward_available = true;
    }
}

void setup() {
    accept_input = true;
    reward_available = false;
    testN = 1;

    Serial.begin(9600);
    genie.Begin(Serial);

    genie.AttachEventHandler(handleButton);

    pinMode(PIN_LED, OUTPUT);
    pinMode(PIN_RELAY, OUTPUT);
    pinMode(PIN_SCREEN, OUTPUT);

    digitalWrite(PIN_SCREEN, HIGH);
    delay(100);
    digitalWrite(PIN_SCREEN, LOW);
    delay(3000);
}

void loop() {
    genie.WriteContrast(15);
    digitalWrite(PIN_LED, LOW);  
    
    unsigned long start = millis();
    int hits = 0;
    accept_input = true;
    
    while (true) {
        genie.DoEvents();

        int now = millis() - start;
        if (now % 1000 == 0) {
            if (now == 10000) {
                genie.WriteContrast(0);
                accept_input = false;
            }
            else if (now == 40000) {
                ++testN;
                break;
            }
        }

        int hit = analogRead(PIN_SWITCH);
        if (hit == 1023) {
            ++hits;
        }
        else {
            hits = 0;
        }       
        if (hits == 100 && reward_available) {
            reward_available = false;
            Serial.println(testN);
        }
    }
}
